-- +---------------+
-- | Davis Nyabuti | 
-- | SEIS 735-01   |
-- | Assignment #7 |
-- +---------------+

# Get CUI for ALS
select distinct CUI from mrconso where str ='Amyotrophic lateral sclerosis';
-- C0002736

# Get Attributes from MRSTY ... looking for TUI so we can join with Semantic Network
select TUI from mrsty where CUI = 'C0002736';
-- T047 (Disease or Syndrome)
-- We will use T047 to join with Semantic Network

# Get CUI for Fluconazole
select distinct CUI from mrconso where str ='Fluconazole';
-- C0016277 OR C0812735 but C0812735 is suppressed

# Get semantic type for Fluconazone
select TUI from mrsty where cui = 'C0016277';
-- T109, T121

# Find UI for treat
select UI from srdef where STY_RL ='treats';
-- T154

# We have Treats = T154, Disease or Syndrom = T047

# Check if Fluconazone can treat a disease
select * from srstre1 
where UI1 in ('T109', 'T121')
and UI2='T154'
and UI3='T047';
-- 1 Row returned


# Now we need to find what can potentially treat ALS
SELECT 
    *
FROM
    mrrel r,
    (SELECT 
        st.CUI
    FROM
        srstre1 s, mrsty st
    WHERE
        s.UI2 = 'T154' AND s.UI3 = 'T047'
            AND st.TUI = s.UI1) d
WHERE
    d.CUI = r.CUI2 AND r.CUI1 = 'C0002736'
        AND r.CUI2 = 'C0016277';
        
# It does not appear that the drug treats ALS

# Treatments options for ALS from mrrel
SELECT DISTINCT
    c.str, c.cui
FROM
    mrrel r,
    mrconso c
WHERE
    r.CUI1 = 'C0002736'
        AND r.RELA = 'may_treat'
        AND r.CUI2 = c.CUI
        AND c.LAT = 'ENG';

# Other treatment options
SELECT 
    distinct c.str, c.cui
FROM
    mrrel r, mrconso c,
    (SELECT 
        st.CUI
    FROM
        srstre1 s, mrsty st
    WHERE
        s.UI2 = 'T154' AND s.UI3 = 'T047'
            AND st.TUI = s.UI1 limit 50000) d
WHERE
    d.CUI = r.CUI2 AND r.CUI1 = 'C0002736' 
    and r.CUI2 = c.CUI
    and r.RELA is null
    and c.LAT= 'ENG' limit 1000;

# Get TUI for signs or symptoms
select distinct TUI from mrsty where sty = 'sign or symptom';
-- T184

# Get CUI for Polydipsia
select distinct CUI from mrconso where str ='Polydipsia';
-- C0085602

# Get CUI for Nutr/metab/devel sym NEC
select distinct CUI from mrconso where str ='Nutr/metab/devel sym NEC';
-- C0159043

# Get CUI for Abnormal weight gain
select distinct CUI from mrconso where str ='Abnormal weight gain';
-- C0332544

# Get CUI for Symptoms concerning nutrition, metabolism, and development
select distinct CUI from mrconso where str ='Symptoms concerning nutrition, metabolism, and development';
-- C0476235

# Get CUI for Lack of expected normal physiological development in childhood
select distinct CUI from mrconso where str ='Lack of expected normal physiological development in childhood';
-- C0878753

# Look use for illness or disease with all the symptoms above by first finding all symptoms in MRREL using TUI
select all_.cui1 from
(select r.CUI1 from mrrel r, mrsty st 
where st.TUI = 'T184'
and r.CUI2 = st.CUI) all_,
(select cui1 from mrrel where cui2='C0085602') poly,
(select cui1 from mrrel where cui2='C0159043')sym,
(select cui1 from mrrel where cui2='C0332544') wg,
(select cui1 from mrrel where cui2='C0476235') dev,
(select cui1 from mrrel where cui2='C0878753')ch
where all_.cui1=poly.cui1
and poly.cui1=sym.cui1
and sym.cui1=wg.cui1
and wg.cui1 = dev.cui1
and dev.cui1=ch.cui1
limit 1;

-- C0003123

# Get name of disease
select STR from mrconso where cui = 'C0003123';

# Verify it is a disease
select STY from mrsty where cui = 'C0003123';
-- Disease or Syndrome
