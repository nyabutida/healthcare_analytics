﻿Davis Nyabuti
SEIS 735-01
Assignment #6
_____________________________________

####  I.
  **1. Determine  which  distinct  diseases  in  the  data  file  are  under  the  SNOMED-CT  category of “Disorder of Nervous System”? ** 
```{sql}
SELECT DISTINCT
    x.str
FROM
    umls.MRCONSO c,
    umls.MRREL r,
    HW.assignment_x x
WHERE
    c.SAB = 'SNOMEDCT_US'
        AND c.STR = 'Disorder of nervous system'
        AND (r.REL = 'CHD' OR r.REL = 'SIB')
        AND x.cui = r.cui2
        AND r.CUI1 = c.CUI;
```
> results
```
+------------------------------------------------+
| str                                            |
+------------------------------------------------+
| Disease affecting entire cardiovascular system |
| Amyotrophic lateral sclerosis                  |
| Alzheimer's disease                            |
| Parkinson's disease                            |
+------------------------------------------------+
4 rows in set (0.31 sec)
```
**2. Determine  how  many  diseases  in  the  data  file  are  under  the  SNOMED-CT  category of “Disorder of Nervous System”? **
```{sql}
SELECT 
    COUNT(x.str)
FROM
    umls.MRCONSO c,
    umls.MRREL r,
    HW.assignment_x x
WHERE
    c.SAB = 'SNOMEDCT_US'
        AND c.STR = 'Disorder of nervous system'
        AND (r.REL = 'CHD' OR r.REL = 'SIB')
        AND x.cui = r.cui2
        AND r.CUI1 = c.CUI;
```
> results
```
+--------------+
| count(x.str) |
+--------------+
|            6 |
+--------------+
1 row in set (0.34 sec)
```
**3. Determine  which  distinct  strings  in  the  data  file  are  under  the  SNOMED-CT  category of “Abdominal Structure”? **
```{sql}
SELECT DISTINCT
    x.str
FROM
    umls.MRCONSO c,
    umls.MRREL r,
    HW.assignment_x x
WHERE
    c.SAB = 'SNOMEDCT_US'
        AND c.STR = 'Abdominal structure'
        AND (r.REL = 'CHD' OR r.REL = 'SIB')
        AND x.cui = r.cui2
        AND r.CUI1 = c.CUI;
        
```
> results
```
+-----------------------+
| str                   |
+-----------------------+
| Gallbladder structure |
| Adrenal gland         |
| Spleen                |
+-----------------------+
3 rows in set (1.98 sec)

```      
**4. Determine how many strings in the data file are under the SNOMED-CT category of “Abdominal Structure”? **
```{sql}
SELECT 
    COUNT(x.str)
FROM
    umls.MRCONSO c,
    umls.MRREL r,
    HW.assignment_x x
WHERE
    c.SAB = 'SNOMEDCT_US'
        AND c.STR = 'Abdominal structure'
        AND (r.REL = 'CHD' OR r.REL = 'SIB')
        AND x.cui = r.cui2
        AND r.CUI1 = c.CUI;
```
> results
```
+--------------+
| COUNT(x.str) |
+--------------+
|            4 |
+--------------+
1 row in set (1.94 sec)
```

#### II.
**1. What are the causative agent(s) of Ebola in SNOMED-CT?**
```{Sql}
SELECT DISTINCT
    c2.STR
FROM
    umls.MRREL rel,
    umls.MRCONSO c,
    umls.MRCONSO c2
WHERE
    c.SAB = 'SNOMEDCT_US'
        AND c.STR = 'Ebola haemorrhagic fever'
        AND c.ISPREF = 'Y'
        AND rel.CUI1 = c.CUI
        AND rel.CUI2 = c2.CUI
        AND rel.RELA = 'causative_agent_of'
        AND c2.TS = 'P'
        AND c2.SAB = 'SNOMEDCT_US'
        AND c2.LAT = 'ENG'
        AND c2.SUPPRESS = 'N';
```
> results
```
+-------------+
| STR         |
+-------------+
| Virus       |
| Viruses     |
| Filoviridae |
| Ebolavirus  |
+-------------+
4 rows in set (0.00 sec)
```        
**2. The causative agent(s) of Ebola also cause what other English medical concepts in ALL vocabulary sources? **
CUI from question 1 are C0042776, C0085150, C0949892
```{sql}
SELECT DISTINCT
    c.STR, c.SAB
FROM
    umls.MRREL rel,
    umls.MRCONSO c
WHERE
    rel.CUI2 IN ('C0042776' , 'C0085150', 'C0949892')
        AND rel.CUI1 = c.CUI
        AND rel.RELA = 'causative_agent_of'
        AND c.str LIKE 'Ebola%'
        AND c.ISPREF = 'Y'
        AND c.LAT = 'ENG'
        AND c.TS = 'P'
        AND c.SUPPRESS = 'N';
        
```
> results
```
+--------------------------+-------------+
| STR                      | SAB         |
+--------------------------+-------------+
| ebola fever hemorrhagic  | CHV         |
| ebola haemorrhagic fever | CHV         |
| Ebola haemorrhagic fever | SNOMEDCT_US |
| ebola hemorrhagic fever  | CHV         |
| Ebola Hemorrhagic Fever  | MSH         |
| Ebola hemorrhagic fever  | SNOMEDCT_US |
+--------------------------+-------------+
```
