
# coding: utf-8

# ### Geoffrey Lamb, Jiaqi Yu, Jialin Yu, Matthew Lipinski, Davis Nyabuti, Huiyu He, Tong Wu
# 
# #### SEIS 735-01 
# 
# #### Assignment #4
# 
# #### October 19, 2017

# In[1]:

import glob
import graphlab as gl

#path =r'/disk1/workspaces/school/healthcare_analytics' # use your path
path =r'/disk3/de_syn_puf/beneficiary'
allFiles = glob.glob(path + "/*.csv")        
ben = gl.SFrame.read_csv(path + "/*Beneficiary*.csv")
print 'All data has been loaded'


# **1. From  all  the  Beneficiary-Summary files,  determine  how  many  patients  have  both the following chronic conditions: depression and Alzheimer's.**

# In[2]:

# filter dataframe on SP_DEPRESSN and SP_ALZHDMTA for values > 0
num_with_dep_and_alz = len(ben[(ben['SP_DEPRESSN'] == 1) & (ben['SP_ALZHDMTA'] == 1)])
print ('Number of patients with Depression and Alzheimer\'s is '+ str(num_with_dep_and_alz))


# **2. From  all  the  Beneficiary-Summary files,  determine  how  many  patients  have  both the following chronic conditions: depression and diabetes**

# In[3]:

# filter dataframe on SP_DEPRESSN and SP_DIABETES for values > 0
num_with_dep_and_diab = len(ben[(ben['SP_DEPRESSN'] == 1) & (ben['SP_DIABETES'] == 1)])
print ('Number of patients with Depression and Diabetes is '+ str(num_with_dep_and_diab))


# **3. From  all  the  Beneficiary-Summary files,  determine  how  many  patients  have  both the following chronic conditions: depression and COPD.**          

# In[4]:

# filter dataframe on SP_DEPRESSN and SP_COPD for values > 0
num_with_dep_and_diab = len(ben[(ben['SP_DEPRESSN'] == 1) & (ben['SP_COPD'] == 1)])
print ('Number of patients with Depression and COPD is '+ str(num_with_dep_and_diab))


# In[7]:

#path =r'/disk1/workspaces/school/healthcare_analytics' # use your path
path =r'/disk3/de_syn_puf/inpatient'       
inpa = gl.SFrame.read_csv(path + "/*Inpatient*.csv", column_type_hints={
        'DESYNPUF_ID':str,'CLM_ID':str,'SEGMENT':int,'CLM_FROM_DT':str
        ,'CLM_THRU_DT':str,'PRVDR_NUM':str,'CLM_PMT_AMT':float,'NCH_PRMRY_PYR_CLM_PD_AMT':float
        ,'AT_PHYSN_NPI':int,'OP_PHYSN_NPI':int,'OT_PHYSN_NPI':int,'CLM_ADMSN_DT':str
        ,'ADMTNG_ICD9_DGNS_CD':str,'CLM_PASS_THRU_PER_DIEM_AMT':float
        ,'NCH_BENE_IP_DDCTBL_AMT':float,'NCH_BENE_PTA_COINSRNC_LBLTY_AM':float
        ,'NCH_BENE_BLOOD_DDCTBL_LBLTY_AM':float,'CLM_UTLZTN_DAY_CNT':int
        ,'NCH_BENE_DSCHRG_DT':str,'CLM_DRG_CD':str,'ICD9_DGNS_CD_1':str
        ,'ICD9_DGNS_CD_2':str,'ICD9_DGNS_CD_3':str,'ICD9_DGNS_CD_4':str
        ,'ICD9_DGNS_CD_5':str,'ICD9_DGNS_CD_6':str,'ICD9_DGNS_CD_7':str
        ,'ICD9_DGNS_CD_8':str,'ICD9_DGNS_CD_9':str,'ICD9_DGNS_CD_10':str
        ,'ICD9_PRCDR_CD_1':str,'ICD9_PRCDR_CD_2':str,'ICD9_PRCDR_CD_3':str
        ,'ICD9_PRCDR_CD_4':str,'ICD9_PRCDR_CD_5':str,'ICD9_PRCDR_CD_6':str
        ,'HCPCS_CD_1':str,'HCPCS_CD_2':str,'HCPCS_CD_3':str,'HCPCS_CD_4':str
        ,'HCPCS_CD_5':str,'HCPCS_CD_6':str,'HCPCS_CD_7':str,'HCPCS_CD_8':str
        ,'HCPCS_CD_9':str,'HCPCS_CD_10':str,'HCPCS_CD_11':str,'HCPCS_CD_12':str
        ,'HCPCS_CD_13':str,'HCPCS_CD_14':str,'HCPCS_CD_15':str,'HCPCS_CD_16':str
        ,'HCPCS_CD_17':str,'HCPCS_CD_18':str,'HCPCS_CD_19':str,'HCPCS_CD_20':str
        ,'HCPCS_CD_21':str,'HCPCS_CD_22':str,'HCPCS_CD_23':str,'HCPCS_CD_24':str
        ,'HCPCS_CD_25':str,'HCPCS_CD_26':str,'HCPCS_CD_27':str,'HCPCS_CD_28':str
        ,'HCPCS_CD_29':str,'HCPCS_CD_30':str,'HCPCS_CD_31':str,'HCPCS_CD_32':str
        ,'HCPCS_CD_33':str,'HCPCS_CD_34':str,'HCPCS_CD_35':str,'HCPCS_CD_36':str
        ,'HCPCS_CD_37':str,'HCPCS_CD_38':str,'HCPCS_CD_39':str,'HCPCS_CD_40':str
        ,'HCPCS_CD_41':str,'HCPCS_CD_42':str,'HCPCS_CD_43':str,'HCPCS_CD_44':str
        ,'HCPCS_CD_45':str
    })
inpa3 = inpa['DESYNPUF_ID','CLM_ID','CLM_FROM_DT','CLM_THRU_DT','CLM_ADMSN_DT'
                ,'NCH_BENE_DSCHRG_DT','ICD9_DGNS_CD_1','ICD9_DGNS_CD_2','ICD9_DGNS_CD_3','ICD9_DGNS_CD_4'
             ,'ICD9_DGNS_CD_5','ICD9_DGNS_CD_6','ICD9_DGNS_CD_7','ICD9_DGNS_CD_8'
             ,'ICD9_DGNS_CD_9','ICD9_DGNS_CD_10']


# In[8]:

# copy specific columns from the sframe and create new columns based on ICD9 Codes

cols_to_copy = ['DESYNPUF_ID','CLM_ID','CLM_FROM_DT','CLM_THRU_DT','CLM_ADMSN_DT'
                ,'NCH_BENE_DSCHRG_DT']
icd9_cols = ['ICD9_DGNS_CD_1','ICD9_DGNS_CD_2','ICD9_DGNS_CD_3','ICD9_DGNS_CD_4'
             ,'ICD9_DGNS_CD_5','ICD9_DGNS_CD_6','ICD9_DGNS_CD_7','ICD9_DGNS_CD_8'
             ,'ICD9_DGNS_CD_9','ICD9_DGNS_CD_10']

# map to keep track of ICD9 code frequency
from collections import defaultdict
icd9_codes = defaultdict(int)

transformed_inpa = gl.SFrame({'DESYNPUF_ID':[str()],'CLM_ID':[str()],'CLM_FROM_DT':[str()]
                              ,'CLM_THRU_DT':[str()],'CLM_ADMSN_DT':[str()]
                              ,'NCH_BENE_DSCHRG_DT':[str()],'IDC9_CODE':[str()]})
transformed = open('transformed_inpa.csv', 'w+')

# write headers
transformed.write("%s\n" % "DESYNPUF_ID,CLM_ID,CLM_FROM_DT,CLM_THRU_DT,CLM_ADMSN_DT,NCH_BENE_DSCHRG_DT,ICD9_CODE")

# write data
def write_file(row):
    for col in icd9_cols:
        # ensure non empty value
        if row[col] != '':
            transformed.write("%s,%s,%s,%s,%s,%s,%s\n" % (row['DESYNPUF_ID']
                                                             ,row['CLM_ID']
                                                             ,row['CLM_FROM_DT']
                                                             ,row['CLM_THRU_DT']
                                                             ,row['NCH_BENE_DSCHRG_DT']
                                                             ,row['CLM_ADMSN_DT']
                                                             ,row[col]))
            icd9_codes[row[col]] +=1  


df = inpa3.to_dataframe() 
df.head(5)
for index, row in df.iterrows():
    write_file(row)
transformed.close()  


# **4. From all the Inpatient files, determine the top 10 ICD code that have the highest frequency.  Please  output  each  code  with  its  frequency  (i.e.  occurrence)  in  the  descending order. **

# In[9]:

# Sort top 10 codes based on frequency from highest to lowest
from collections import OrderedDict
codes_desc = sorted(icd9_codes.items(), key=lambda kv: kv[1], reverse=True)
count = 0
for x in codes_desc:
    count +=1
    print x[0] , x[1]
    if count == 10:
        break


# In[11]:

# Load saved file into a new SFrame
path2 =r'/disk1/workspaces/school/healthcare_analytics'
inpa2 = gl.SFrame.read_csv(path2 + "/transformed_inpa.csv", column_type_hints={
        'DESYNPUF_ID':str,'CLM_ID':str,'CLM_FROM_DT':str,'CLM_THRU_DT':str
        ,'CLM_ADMSN_DT':str ,'NCH_BENE_DSCHRG_DT':str,'IDC9_CODE':str})

# # Set empty date columns to 1970-01-01
inpa2['CLM_FROM_DT'] = inpa2['CLM_FROM_DT'].apply(lambda x: '19700101' if x == '' else x)
inpa2['CLM_THRU_DT'] = inpa2['CLM_THRU_DT'].apply(lambda x: '19700101' if x == '' else x)
inpa2['CLM_ADMSN_DT'] = inpa2['CLM_ADMSN_DT'].apply(lambda x: '19700101' if x == '' else x)
inpa2['NCH_BENE_DSCHRG_DT'] = inpa2['NCH_BENE_DSCHRG_DT'].apply(lambda x: '19700101' if x == '' else x)

# # Convert date columns to date
inpa2['CLM_FROM_DT'] = inpa2['CLM_FROM_DT'].str_to_datetime("%Y%m%d")
inpa2['CLM_THRU_DT'] = inpa2['CLM_THRU_DT'].str_to_datetime("%Y%m%d")
inpa2['CLM_ADMSN_DT'] = inpa2['CLM_ADMSN_DT'].str_to_datetime("%Y%m%d")
inpa2['NCH_BENE_DSCHRG_DT'] = inpa2['NCH_BENE_DSCHRG_DT'].str_to_datetime("%Y%m%d")


# In[36]:

# Use a sliding window of 90 days to determine the highest readmission freq
# Start from 2008 and slide window until 2010

import datetime
from datetime import timedelta
import graphlab.aggregate as agg
s_min_date = '2008-01-01'
s_max_date = '2010-12-31'
min_date = datetime.datetime.strptime(s_min_date, "%Y-%m-%d")
max_date = datetime.datetime.strptime(s_max_date, "%Y-%m-%d")

#create window
window = 90
window_min = min_date
window_max = min_date + datetime.timedelta(days=window)

grouped = gl.SFrame({'DESYNPUF_ID': [str()], 'ICD9_CODE': [str()],'COUNT':[int()]})

while window_min < max_date:
    
    inpa_filter = inpa2[(inpa2['CLM_FROM_DT'] <= window_max)
                        & (inpa2['CLM_FROM_DT'] >= window_min)]
    inpa_filter_grouped = inpa_filter.groupby(
        key_columns=['DESYNPUF_ID','ICD9_CODE']
                                             ,operations={'COUNT': agg.COUNT()})
    grouped = grouped.append(inpa_filter_grouped)
        
    window_min += datetime.timedelta(days=1)
    window_max += datetime.timedelta(days=1)
    if window_max > max_date:
        window_max = max_date


# In[35]:

# create an sframe with distinct values
grouped_unique = grouped.unique()
# patients with the highest readmission frequency
grouped_unique.sort('COUNT', ascending = False).head(10)


# In[39]:

# Codes with the highest readmission frequency
code_freq = grouped_unique['ICD9_CODE','COUNT']
highest_code_freq = code_freq.groupby(
        key_columns=['ICD9_CODE'],operations={'FREQ': agg.MAX('COUNT')})


# **5. From all the Inpatient files and other appropriate files, determine the top 10 ICD code that have the highest re-admission frequency in 90 days.**

# In[40]:

highest_code_freq.sort('FREQ', ascending = False).head(10)


# In[ ]:



